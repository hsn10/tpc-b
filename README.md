# TPC-B

BSD Licensed [TPC-B](http://www.tpc.org/tpc_documents_current_versions/pdf/tpc-b_v2.0.0.pdf) JDBC benchmark.

#### Supported databases
1. IBM DB/2 Linux, Unix, Windows
1. Apache Derby
1. PostgreSQL
1. Oracle
1. MySQL
1. Generic JDBC

Some collected []