name := "tpcb"
version := "1.1-SNAPSHOT"

javaSource in Compile := baseDirectory.value / "src"
target := baseDirectory.value / "bin"

compileOrder := CompileOrder.JavaThenScala
autoScalaLibrary := false
crossPaths := false

javacOptions in (Compile,compile) ++= Seq ("-source","1.7","-target","1.7")
